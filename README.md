# Config

Faire une graph de l'inventaire : 
```bash
ansible-inventory --inventory inventories/ --graph
```
Execution d'un playbook : 
```bash
ansible-playbook playbook.yml
```
Verifier syntaxe playbook
```bash
ansible-playbook --syntax-check playbook.yml
```
Ping la machine node-01.workshop (ping ansible != ping ICMP)
```bash
ansible node-01.workshop -m ping
```
Executer la commande "id" sur la machine node-01.workshop
```bash
ansible node-01.workshop -m command -a "id"
```
Executer la commande "uname -r" sur la machine node-01.workshop
```bash
ansible node-01.workshop -m command -a "uname -r"
```
Installer la derniere version de "tcpdump" sur la machine node-01.workshop
```bash
ansible node-01.workshop -m apt -a 'name=tcpdump state=latest' -b
```

https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#variable-precedence-where-should-i-put-a-variable

https://docs.ansible.com/ansible/2.3/intro_configuration.html#gathering
